#!/bin/bash
eval $(cat vm.gitlab.config)
multipass delete ${vm_name}
multipass purge

rm  config/gitlab.hosts.config
rm  config/export.gitlab.config

eval $(cat vm.k3s.config)
multipass delete ${vm_name}
multipass purge

rm  config/export.cluster.config
rm  config/k3s.yaml
rm  config/ca.txt
rm  config/token.txt