#!/bin/bash
echo "🤖 starting K3S virtual machine..."
eval $(cat vm.k3s.config)
multipass start ${vm_name}

echo "🦊 starting GitLab virtual machine..."
eval $(cat vm.gitlab.config)
multipass start ${vm_name}

. fix-coredns.sh
