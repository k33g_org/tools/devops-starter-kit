# DevOps starter-kit

This project provides a set of scripts to
- Install two **[Multipass](https://multipass.run/)** virtual machines with:
  - GitLab + [container registry](https://docs.gitlab.com/ee/user/packages/container_registry/index.html)
  - K3S mono cluster (a Kubernetes distribution from Rancher)
- Provide the configuration data to set up the [Kubernetes integration with GitLab](https://docs.gitlab.com/ee/user/project/clusters/index.html)

## Requirements

- Install **[Multipass](https://multipass.run/)**

## 🚀 run the VMs creation

- You need to `git clone` this repository:
  - `git clone git@gitlab.com:k33g_org/tools/devops-starter-kit.git`
- *[Optional]* You can change the values (VMs sizing) in `vm.gitlab.config` and `vm.k3s.config`
- Then, run:
  ```
  cd devops-starter-kit
  ./create-vms.sh
  ```
- Wait some minutes... ⏳

> **Remark**: if you want to install an enterprise edition of GitLab:
> - edit `vm.gitlab.config`
> - change this value `gitlab_edition="gitlab-ce";` by `gitlab_edition="gitlab-ee";`
> - save it

## GitLab instance setup

- Once the VMs created and running, update your `hosts` file with the values of `config/gitlab.hosts.config`, for example
  ```
  192.168.64.119 gitlab.december.test 
  ```
- Connect to http://gitlab.december.test
  - give a password for the `root` user
  - connect as `root` user (admin)
  - go to http://gitlab.december.test/admin/application_settings/network
    - expand the "Outbound requests" section
    - check *Allow requests to the local network from web hooks and services*
    - (if not) check *Allow requests to the local network from system hooks*
    - click on <kbd>Saves changes</kbd>
  - go to http://gitlab.december.test/admin/application_settings/ci_cd
    - expand the "Continuous Integration and Deployment" section
    - uncheck *Default to Auto DevOps pipeline for all projects*
    - click on <kbd>Saves changes</kbd>

## Kubernetes Integration setup

We'll connect the K3S cluster at the instance level, but you can do it at the group (or project) level.

- go to http://gitlab.december.test/admin/clusters
  - click on <kbd>Integrate with a cluster certificate</kbd>
  - select the <kbd>Connect existing cluster</kbd> tab
  - fill the form:
    - **Kubernetes cluster name**: `k3s-mono-cluster` *(you can type what you want of course)*
    - **API URL**: `https://192.168.64.121:6443` *(you can find it in the `config/k3s.yaml` file at the `server` section)*
    - **CA Certificate**: copy/paste the content of `config/ca.txt`
    - **Service Token**: copy/paste the content of `config/token.txt`
  - click on <kbd>Add Kubernetes cluster</kbd>
  - on the next form (*Details*) fill **Base domain** with `192.168.64.121.nip.io` (*the cluster IP + `.nip.io`, `.xip.io` should work too*)
  - click on <kbd>Save changes</kbd>

## Add Prometheus and the Kubernetes executor

- Select the <kbd>Applications</kbd> tab
  - click on <kbd>Install</kbd> for **Prometheus**
  - click on <kbd>Install</kbd> for **GitLab Runner**
  - wait for a moment ⏳
- That's all 🎉

## Other scripts

- `stop-vms.sh`: stop the 2 virtual machines
- `start-vms.sh`: start the 2 virtual machines
- `destroy-vms.sh`: remove/drop the 2 virtual machines
- `shell-gitlab-vm.sh`: ssh connect to the GitLab instance
- `shell-k3s-vm.sh`: ssh connect to the K3S instance

## Any questions?

- https://gitlab.com/k33g_org/tools/devops-starter-kit/-/issues

## Everyone can contribute

- https://gitlab.com/k33g_org/tools/devops-starter-kit/-/merge_requests

## Tested

| OS                  | Status |
| ------------------- | ------ |
| OSX (BigSur)        | ✅     |
| OSX (Catalina)      | ✅     |
| Linux (Ubuntu 20.4) | ✅     |
| Windows             | 🤔     |

## Tips

- Add your ssh key to the GitLab instance:
  - copy the content of your public ssh key
    ```
    cat ~/.ssh/id_rsa.pub
    ```
  - go to your GitLab user's settings
  - select the **SSH Keys** option in the left menu panel
  - add a new key and paste the content of your public ssh key  
- 🖐️ Keep the "dockerized" projects public, if you want to deploy them on K3S, because K3S needs to see the Docker registry of the project (I need to investigate on how to use private registry)

## Samples

> 🚧 This is a work in progress
