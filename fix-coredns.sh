#!/bin/bash
# read the configuration of the GitLab instance
eval $(cat config/export.gitlab.config)
gitlab_ip=${vm_ip}
gitlab_domain=${vm_domain}

# read the configuration of the K3S instance
eval $(cat config/export.cluster.config)
cluster_vm_name=${vm_name}

echo "👋 Setup 🦊 integration"
echo "📝 apply some change to coredns"

. coredns-template.sh

multipass --verbose exec ${cluster_vm_name} -- bash <<EOF
sudo chmod -R 777 /etc/rancher/k3s/k3s.yaml

kubectl wait --for=condition=available deployment/coredns -n kube-system 
kubectl wait --for=condition=available deployment/local-path-provisioner -n kube-system  
kubectl wait --for=condition=available deployment/metrics-server -n kube-system  

echo "$TEMPLATE" > coredns.patch.yaml

sed -i "s/GITLAB_IP/${gitlab_ip}/" coredns.patch.yaml
sed -i "s/GITLAB_DOMAIN/${gitlab_domain}/" coredns.patch.yaml

kubectl get configmap coredns -n kube-system -o yaml > coredns.yaml

corefilepatch=\$(yq read coredns.patch.yaml  'data.Corefile')

yq delete -i coredns.yaml 'data.Corefile'
yq write -i coredns.yaml 'data.Corefile' "\${corefilepatch}"

kubectl apply -f coredns.yaml
kubectl delete pod --selector=k8s-app=kube-dns -n kube-system

#cat coredns.yaml
EOF

