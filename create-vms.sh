#!/bin/bash

# -----------------------------------------------
#  GitLab setup
# -----------------------------------------------
eval $(cat vm.gitlab.config)

multipass launch --name ${vm_name} \
  --cpus ${vm_cpus} \
	--mem ${vm_mem} \
	--disk ${vm_disk} \
	--cloud-init ./gitlab.cloud-init.yaml

IP=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')

echo "👋 Initialize ${vm_name}..."

multipass info ${vm_name}

multipass exec ${vm_name}-- sudo -- sh -c "echo \"${IP} ${vm_domain}\" >> /etc/hosts"

multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
apt-get update
curl -LO https://packages.gitlab.com/install/repositories/gitlab/${gitlab_edition}/script.deb.sh
bash ./script.deb.sh
EXTERNAL_URL='http://${vm_domain}' apt-get install -y ${gitlab_edition}
EOF

# 🖐 add this to `hosts` file(s)
echo "${IP} ${vm_domain}" > config/gitlab.hosts.config

# 🖐 use this file to exchange data between VM creation script
# use: eval $(cat ../registry/workspace/export.registry.config)
target="config/export.gitlab.config"
echo "vm_name=\"${vm_name}\";" >> ${target}
echo "vm_domain=\"${vm_domain}\";" >> ${target}
echo "vm_ip=\"${IP}\";" >> ${target}

# -----------------------------------------------
#  GitLab Docker registry activation
# -----------------------------------------------

# https://docs.gitlab.com/ee/administration/packages/container_registry.html#configure-container-registry-under-an-existing-gitlab-domain
multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
apt install rpl -y
rpl "# registry_external_url 'https://registry.example.com'" "registry_external_url 'http://${vm_domain}:5005'" /etc/gitlab/gitlab.rb
gitlab-ctl reconfigure
EOF

# -----------------------------------------------
#  K3S Mono cluster setup
# -----------------------------------------------

eval $(cat vm.k3s.config)

multipass launch --name ${vm_name} \
  --cpus ${vm_cpus} \
	--mem ${vm_mem} \
	--disk ${vm_disk} \
	--cloud-init ./k3s.cloud-init.yaml

# Initialize K3s on node
echo "👋 Initialize 📦 K3s on ${vm_name}..."

multipass info ${vm_name}

# install k3s
multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
curl -sfL https://get.k3s.io | sh -
EOF

IP=$(multipass info ${vm_name} | grep IPv4 | awk '{print $2}')

echo "😃 📦 K3s initialized on ${vm_name} ✅"
echo "🖥 IP: ${IP}"

multipass --verbose exec ${vm_name} -- sudo -- bash <<EOF
sed "s/127.0.0.1/$IP/" /etc/rancher/k3s/k3s.yaml  > /tmp/k3s.yaml
chown ubuntu /tmp/k3s.yaml
snap install yq

kubectl wait --for=condition=available deployment/coredns -n kube-system 
kubectl wait --for=condition=available deployment/local-path-provisioner -n kube-system  
kubectl wait --for=condition=available deployment/metrics-server -n kube-system  
echo "🎉 K3S installation complete 🍾"
EOF

multipass transfer ${vm_name}:/tmp/k3s.yaml config/k3s.yaml

# 🖐 use this file to exchange data between VM creation script
target="config/export.cluster.config"
echo "vm_name=\"${vm_name}\";" >> ${target}
echo "vm_domain=\"${vm_domain}\";" >> ${target}
echo "vm_ip=\"${IP}\";" >> ${target}

# -----------------------------------------------
#  Update GitLab VM hosts file
# -----------------------------------------------

# read the configuration of the GitLab instance
eval $(cat config/export.gitlab.config)
gitlab_ip=${vm_ip}
gitlab_domain=${vm_domain}
gitlab_vm_name=${vm_name}

# read the configuration of the K3S instance
eval $(cat config/export.cluster.config)
cluster_vm_name=${vm_name}

echo "👋 Setup 🦊 integration - update hosts file"
echo "📝 update of the /etc/hosts file of the cluster instance"
multipass --verbose exec ${cluster_vm_name} -- sudo -- bash <<EOF
echo "${gitlab_ip} ${gitlab_domain}" >> /etc/hosts
EOF

# -----------------------------------------------
#  Update coredns
# -----------------------------------------------

echo "📝 apply some change to coredns"

. coredns-template.sh

multipass --verbose exec ${cluster_vm_name} -- bash <<EOF
echo "$TEMPLATE" > coredns.patch.yaml

sed -i "s/GITLAB_IP/${gitlab_ip}/" coredns.patch.yaml
sed -i "s/GITLAB_DOMAIN/${gitlab_domain}/" coredns.patch.yaml

sudo chmod -R 777 /etc/rancher/k3s/k3s.yaml

kubectl get configmap coredns -n kube-system -o yaml > coredns.yaml

corefilepatch=\$(yq read coredns.patch.yaml  'data.Corefile')

yq delete -i coredns.yaml 'data.Corefile'
yq write -i coredns.yaml 'data.Corefile' "\${corefilepatch}"

kubectl apply -f coredns.yaml
kubectl delete pod --selector=k8s-app=kube-dns -n kube-system

EOF

# -----------------------------------------------
#  Create the GitLab admin service account
# -----------------------------------------------

# Refs:
# - https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#existing-kubernetes-cluster

# read the configuration of the K3S instance
eval $(cat config/export.cluster.config)
cluster_vm_name=${vm_name}

read -d '' SERVICE_ACCOUNT << EOF
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab-admin
  namespace: kube-system
---
#apiVersion: rbac.authorization.k8s.io/v1beta1
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: gitlab-admin
  namespace: kube-system
EOF

# Create the service account
multipass --verbose exec ${cluster_vm_name} -- bash <<EOF
echo "$SERVICE_ACCOUNT" > gitlab-admin-service-account.yaml
kubectl apply -f gitlab-admin-service-account.yaml
EOF

# -----------------------------------------------
#  Get certificate(s) and token
# -----------------------------------------------

multipass --verbose exec ${cluster_vm_name} -- bash <<EOF
grep 'certificate' /etc/rancher/k3s/k3s.yaml | awk -F ': ' '{print \$2}' | base64 -d > ca.txt
SECRET=\$(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print \$1}')
kubectl -n kube-system get secret \$SECRET -o jsonpath='{.data.token}' | base64 -d > token.txt
EOF

multipass transfer ${cluster_vm_name}:ca.txt config/ca.txt
multipass transfer ${cluster_vm_name}:token.txt config/token.txt

# -----------------------------------------------
#  Declare the registry as insecure
# -----------------------------------------------
registry_domain=${gitlab_domain}
registry_port=5005
target="/etc/rancher/k3s/registries.yaml"

read -r -d '' cmd_insecure << EOM
	mkdir -p /etc/rancher/k3s
	echo 'mirrors:'  >> ${target}
	echo '  \"${registry_domain}:${registry_port}\":' >> ${target}
	echo '    endpoint:'  >> ${target}
	echo '      - \"http://${registry_domain}:${registry_port}\"' >> ${target}
EOM

multipass --verbose exec ${cluster_vm_name} -- sudo -- sh -c "${cmd_insecure}"

echo "🚀 stoping the VMs..."
multipass stop ${cluster_vm_name}
multipass stop ${gitlab_vm_name}

echo "🚀 restarting the VMs..."
. start-vms.sh


